#!/bin/bash
export WEBDEV_PATH=/opt/webdev-env/
export ROOT_DOMAIN=bmoine.fr
export DOMAINS="git registry vps www hook go"
export EMAIL=contact@bmoine.fr

pushd ${WEBDEV_PATH}
docker-compose stop http
pushd /opt/certbot/
for D in ${DOMAINS}; do
    DOMAIN=${D}.${ROOT_DOMAIN}
    openssl ecparam -genkey -name secp384r1 -outform PEM -out /tmp/${DOMAIN}_ecdsa.key
    openssl req -new -sha384 -key /tmp/${DOMAIN}_ecdsa.key -subj "/CN=${DOMAIN}" -outform DER -out /tmp/${DOMAIN}_ecdsa.der
    ./certbot-auto certonly \
        --authenticator standalone \
        --agree-tos \
        --preferred-challenges http \
        --email ${EMAIL} \
        --eff-email \
        --force-renew \
        --cert-path /tmp/${DOMAIN}_ecdsa.pem \
        --csr /tmp/${DOMAIN}_ecdsa.der \
        --chain-path /tmp/${DOMAIN}_chain.pem \
        --fullchain-path /tmp/${DOMAIN}_fullchain.pem \
        --domain ${DOMAIN}
    pushd /tmp/
    export X1_PIN_SHA256=$(curl --silent --url https://letsencrypt.org/certs/isrgrootx1.pem | openssl x509 -pubkey | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64)
    export X3_PIN_SHA256=$(curl --silent --url https://letsencrypt.org/certs/letsencryptauthorityx3.pem | openssl x509 -pubkey | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64)
    export X4_PIN_SHA256=$(curl --silent --url https://letsencrypt.org/certs/letsencryptauthorityx4.pem | openssl x509 -pubkey | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64)
    openssl x509 -noout -in /tmp/${DOMAIN}_ecdsa.pem -pubkey | openssl asn1parse -noout -inform pem -out public.key
    export PIN_SHA256=$(openssl dgst -sha256 -binary public.key | openssl enc -base64)
    rm public.key
    perl -p -i -e 's@{{X1_PIN_SHA256}}@'${X1_PIN_SHA256}'@' ${WEBDEV_PATH}/conf/nginx/${D}_common.conf
    perl -p -i -e 's@{{X3_PIN_SHA256}}@'${X3_PIN_SHA256}'@' ${WEBDEV_PATH}/conf/nginx/${D}_common.conf
    perl -p -i -e 's@{{X4_PIN_SHA256}}@'${X4_PIN_SHA256}'@' ${WEBDEV_PATH}/conf/nginx/${D}_common.conf
    perl -p -i -e 's@{{PIN_SHA256}}@'${PIN_SHA256}'@' ${WEBDEV_PATH}/conf/nginx/${D}_common.conf
    mv /tmp/${DOMAIN}_* ${WEBDEV_PATH}/data/tls/
    popd
done
popd
unset WEBDEV_PATH ROOT_DOMAIN DOMAINS EMAIL D DOMAIN
docker-compose up -d

