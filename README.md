Development environment based on PHP-FPM, Nginx and MariaDB
===========================================================

## Informations ##

### File Structure ###

	data        # Directory linked to VirtualBox API
	├─── build  # Data used to build containers (i.e., DockerFile)
	├─── conf   # Configuration data used by services
	├─── data   # Raw data used by services
	└─── log    # Logs issued by services

## Setup ##

### Requirements ###

- [Docker](https://docs.docker.com/engine/installation/);
- [Docker Compose](https://docs.docker.com/compose/install/);
- Less time to prepare environment, more time to develop.

More information [here](https://gitlab.com/Creased/vps).

### Download ###

```bash
git clone https://gitlab.com/Creased/vps-webdev-env webdev-env
pushd webdev-env/
git submodule init
git submodule update
export WEBDEV_PATH=$(pwd)
```

### Build ###

Build of containers based on docker-compose.yml:

```bash
docker-compose pull
docker-compose build
```

### Certificates ###

Install Let's Encrypt:

```bash
apt-get install -y python3-pip
pushd /opt/
git clone https://github.com/certbot/certbot
pushd certbot/
./certbot-auto --os-packages-only --non-interactive
```

Set variables:

```bash
export DOMAIN=YOUR_FQDN
export EMAIL=YOUR_EMAIL
```

Generate certificate:

```bash
openssl ecparam -genkey -name secp384r1 -outform PEM -out ${WEBDEV_PATH}/data/tls/${DOMAIN}_ecdsa.key
openssl req -new -sha384 -key ${WEBDEV_PATH}/data/tls/${DOMAIN}_ecdsa.key -subj "/CN=${DOMAIN}" -outform DER -out ${WEBDEV_PATH}/data/tls/${DOMAIN}_ecdsa.der
./certbot-auto certonly \
    --authenticator standalone \
    --agree-tos \
    --preferred-challenges http \
    --email ${EMAIL} \
    --eff-email \
    --force-renew \
    --cert-path ${WEBDEV_PATH}/data/tls/${DOMAIN}_ecdsa.pem \
    --csr ${WEBDEV_PATH}/data/tls/${DOMAIN}_ecdsa.der \
    --chain-path ${WEBDEV_PATH}/data/tls/${DOMAIN}_chain.pem \
    --fullchain-path ${WEBDEV_PATH}/data/tls/${DOMAIN}_fullchain.pem \
    --domain ${DOMAIN}
popd
popd
```

Add HTTP Public Key Pinning (HPKP):

```bash
pushd /tmp/
export X1_PIN_SHA256=$(curl --silent --url https://letsencrypt.org/certs/isrgrootx1.pem | openssl x509 -pubkey | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64)
export X3_PIN_SHA256=$(curl --silent --url https://letsencrypt.org/certs/letsencryptauthorityx3.pem | openssl x509 -pubkey | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64)
export X4_PIN_SHA256=$(curl --silent --url https://letsencrypt.org/certs/letsencryptauthorityx4.pem | openssl x509 -pubkey | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64)
openssl x509 -noout -in ${WEBDEV_PATH}/data/tls/${DOMAIN}_ecdsa.pem -pubkey | openssl asn1parse -noout -inform pem -out public.key
export PIN_SHA256=$(openssl dgst -sha256 -binary public.key | openssl enc -base64)
rm public.key
perl -p -i -e 's@{{X1_PIN_SHA256}}@'${X1_PIN_SHA256}'@' ${WEBDEV_PATH}/conf/nginx/common.conf
perl -p -i -e 's@{{X3_PIN_SHA256}}@'${X3_PIN_SHA256}'@' ${WEBDEV_PATH}/conf/nginx/common.conf
perl -p -i -e 's@{{X4_PIN_SHA256}}@'${X4_PIN_SHA256}'@' ${WEBDEV_PATH}/conf/nginx/common.conf
perl -p -i -e 's@{{PIN_SHA256}}@'${PIN_SHA256}'@' ${WEBDEV_PATH}/conf/nginx/common.conf
unset DOMAIN EMAIL WEBDEV_PATH
popd
```

## Start ##

To get it up, please consider using:

```bash
docker-compose up -d
```

### Browse your applications ###

When done, turn on your web browser and crawl your VPS (e.g., [https://vps.bmoine.fr/](https://vps.bmoine.fr/)).

## Live display of logs ##

```bash
docker-compose logs --follow
```

## Run bash on container ##

Template:

```bash
docker-compose exec SERVICE COMMAND
```

Example:

```bash
docker-compose exec db bash
```

Then you will be able to manage your configuration files, debug daemons and much more...

## Renewal of certificates ##

```bash
./renew.sh
```
