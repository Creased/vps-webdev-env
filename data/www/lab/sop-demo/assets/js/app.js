$(document).ready(function() {
    $('#sop-test .url').each(function () {
        var url = $(this).text();
        var output = $(this).next('.result');

        $.ajax({
            url: url,
            success: function (data, status, jqXHR) {
                output.text("\u2714\ufe0f");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                output.text("\ud83d\udeab");
            }
        });
    });
});
