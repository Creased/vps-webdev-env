#!/bin/bash
export CERT_PATH=${CERT_PATH:-/etc/ssl/private}
export HOSTNAME=${HOSTNAME:-hostname}
export DOMAIN=${DOMAIN:-domain}
export EMAIL=${EMAIL:-mail@domain}
export CERTBOT=/opt/certbot/certbot-auto

if [ ! "${HOSTNAME}" == "hostname" ] && [ ! "${DOMAIN}" == "domain" ] && [ ! "${EMAIL}" == "mail@domain" ]; then
    ###
    # Let's Encrypt
    #
    touch /var/log/letsencrypt/letsencrypt.log
    for HOST in ${HOSTNAME}; do
        if [ ! -f ${CERT_PATH}/${HOST}.${DOMAIN}_fullchain.pem ]; then
            openssl ecparam -genkey -name secp384r1 -outform PEM -out ${CERT_PATH}/${HOST}.${DOMAIN}_ecdsa.key
            openssl req -new -sha384 -key ${CERT_PATH}/${HOST}.${DOMAIN}_ecdsa.key -subj "/CN=${HOST}.${DOMAIN}" -outform DER -out ${CERT_PATH}/${HOST}.${DOMAIN}_ecdsa.der
            ${CERTBOT} certonly --no-bootstrap --non-interactive \
                                --authenticator standalone \
                                --agree-tos \
                                --preferred-challenges http \
                                --email ${EMAIL} \
                                --eff-email \
                                --force-renew \
                                --cert-path ${CERT_PATH}/${HOST}.${DOMAIN}_ecdsa.pem \
                                --csr ${CERT_PATH}/${HOST}.${DOMAIN}_ecdsa.der \
                                --chain-path ${CERT_PATH}/${HOST}.${DOMAIN}_chain.pem \
                                --fullchain-path ${CERT_PATH}/${HOST}.${DOMAIN}_fullchain.pem \
                                --domain ${HOST}.${DOMAIN}
        else
            ${CERTBOT} renew --no-bootstrap --non-interactive
        fi

        cp /etc/nginx/${HOST}_common.conf /tmp/common.conf
        sed -i -r 's@(add_header[ \t]+Public-Key-Pins ['"'"']).+(max-age=[0-9]+[^'"'"']*['"'"'] always;)@\1pin-sha256="{{TRUSTID_PIN_SHA256}}"; pin-sha256="{{X1_PIN_SHA256}}"; pin-sha256="{{X3_PIN_SHA256}}"; pin-sha256="{{X4_PIN_SHA256}}"; pin-sha256="{{PIN_SHA256}}"; \2@g' /tmp/common.conf
        cd /tmp/
        export TRUSTID_PIN_SHA256=$(curl --silent --url https://www.identrust.com/doc/SSLTrustIDCAA5_DSTCAX3.p7b | openssl pkcs7 -outform PEM -print_certs | awk '/subject.*CN=DST Root CA X3/,/END CERTIFICATE/' | openssl x509 -pubkey | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64)
        export X1_PIN_SHA256=$(curl --silent --url https://letsencrypt.org/certs/isrgrootx1.pem | openssl x509 -pubkey | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64)
        export X3_PIN_SHA256=$(curl --silent --url https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem | openssl x509 -pubkey | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64)
        export X4_PIN_SHA256=$(curl --silent --url https://letsencrypt.org/certs/lets-encrypt-x4-cross-signed.pem | openssl x509 -pubkey | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64)
        openssl x509 -noout -in ${CERT_PATH}/${HOST}.${DOMAIN}_ecdsa.pem -pubkey | openssl asn1parse -noout -inform pem -out public.key
        export PIN_SHA256=$(openssl dgst -sha256 -binary public.key | openssl enc -base64)
        rm public.key
        perl -p -i -e 's@{{TRUSTID_PIN_SHA256}}@'${TRUSTID_PIN_SHA256}'@' /tmp/common.conf
        perl -p -i -e 's@{{X1_PIN_SHA256}}@'${X1_PIN_SHA256}'@' /tmp/common.conf
        perl -p -i -e 's@{{X3_PIN_SHA256}}@'${X3_PIN_SHA256}'@' /tmp/common.conf
        perl -p -i -e 's@{{X4_PIN_SHA256}}@'${X4_PIN_SHA256}'@' /tmp/common.conf
        perl -p -i -e 's@{{PIN_SHA256}}@'${PIN_SHA256}'@' /tmp/common.conf
        cp /tmp/common.conf /etc/nginx/${HOST}_common.conf
        rm /tmp/common.conf
    done

    ###
    # nginx
    #
    nginx -g "daemon on;"

    ##
    # logs
    #
    tail -F /usr/share/nginx/log/*.log
else
    printf "Please define your contextual variable (hostname, domain, email) in .env file\x21\n"
fi
